#include "instructionUtil.h"
#include "machine.h"
#include "main.h"

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

/**
 * Execute instruction given
 * Returns change in pc
 */
int32_t runInstruction(uint32_t instruction)
{
    int32_t *reg = parseRegisters(instruction);
    int32_t instructionIndex = findInstruction(instruction);

    int32_t returnVal = 0;
    int32_t rd = -1;
    int32_t rn = -1;
    int32_t rm = -1;
    int32_t adr = -1;
    int32_t im = -1;
    int32_t shamt = -1;

    switch (instructionIndex)
    {
        case ADD: // rd = rn + rm
        {  
            rd = reg[0];
            rn = reg[1];
            rm = reg[2];
            machineState.registers[rd] = machineState.registers[rn] + machineState.registers[rm];
            returnVal = 1;
            break;
        }
        case ADDI: // rd = rn + #im
        {
            rd = reg[0];
            rn = reg[1];
            im = reg[2];
            machineState.registers[rd] = machineState.registers[rn] + im;
            returnVal = 1;
            break;
        }
        case AND: // AND
        {
            rd = reg[0];
            rn = reg[1];
            rm = reg[2];
            machineState.registers[rd] = machineState.registers[rn] & machineState.registers[rm];
            returnVal = 1;
            break;
        }
        case ANDI: // ANDI
        {
            rd = reg[0];
            rn = reg[1];
            im = reg[2];
            machineState.registers[rd] = machineState.registers[rn] & im;
            returnVal = 1;
            break;
        }
        case B: // B
        {
            adr = reg[0];
            returnVal = adr;
            break;
        }
        case Bcond: // Bcond
        {
            rd = reg[0];
            adr = reg[1];
            if (machineState.conditionRegisters[rd])
            {
                returnVal = adr;
            }
            else
            {
                returnVal = 1;
            }
            controlHazards++;
            break;
        }
        case BL: // BL
        {
            adr = reg[0];
            machineState.registers[30] = machineState.pc + 1; // X30 = LR
            returnVal = adr;
            break;
        }
        case BR: // BR
        {
            rd = reg[0];
            returnVal = machineState.registers[30] - machineState.pc; // X30 = LR
            break;
        }
        case CBNZ: // CBNZ
        {
            rd = reg[0];
            adr = reg[1];
            returnVal = machineState.registers[rd] != 0 ? adr : 1;
            controlHazards++;
            break;
        }
        case CBZ: // CBZ
        {
            rd = reg[0];
            adr = reg[1];
            returnVal = machineState.registers[rd] == 0 ? adr : 1;
            controlHazards++;
            break;
        }
        case DUMP: // Prints registers, memory, and stack to stdout
        {
            printDump();
            returnVal = 1;
            break;
        }
        case EOR: // EOR
        {
            rd = reg[0];
            rn = reg[1];
            rm = reg[2];
            machineState.registers[rd] = machineState.registers[rn] ^ machineState.registers[rm];
            returnVal = 1;
            break;
        }
        case EORI: // EORI
        {
            rd = reg[0];
            rn = reg[1];
            im = reg[2];
            machineState.registers[rd] = machineState.registers[rn] | im;
            returnVal = 1;
            break;
        }
        case HALT: // HALT
        {
            printDump();
            machineState.pc = -1; // End program
            returnVal = 0;
            break;
        }
        case LDUR: // LDUR
        {
            rd = reg[0];
            rn = reg[1];
            adr = reg[2];
            memcpy(&machineState.registers[rd],
                machineState.memory + machineState.registers[rn] + adr, 
                sizeof(machineState.memory + machineState.registers[rn] + adr)
            );
            returnVal = 1;
            break;
        }
        case LDURB: // NOT REQUIRED; emulator will skip this instruction
        {
            returnVal = 1;
            break;
        }
        case LDURH: // NOT REQUIRED; emulator will skip this instruction
        {
            returnVal = 1;
            break;
        }
        case LDURSW: // NOT REQUIRED; emulator will skip this instruction
        {
            returnVal = 1;
            break;
        }
        case LSL: // LSL
        {
            rd = reg[0];
            rn = reg[1];
            // uint32_t rm = reg[2]; // rm unused
            shamt = reg[3];
            machineState.registers[rd] = machineState.registers[rn] << shamt;
            returnVal = 1;
            break;
        }
        case LSR: // LSR
        {
            rd = reg[0];
            rn = reg[1];
            // uint32_t rm = reg[2]; // rm unused
            shamt = reg[3];
            machineState.registers[rd] = machineState.registers[rn] >> shamt;
            returnVal = 1;
            break;
        }
        case MUL: // MUL
        {
            rd = reg[0];
            rn = reg[1];
            rm = reg[2];
            machineState.registers[rd] = machineState.registers[rn] * machineState.registers[rm];
            returnVal = 1;
            break;
        }
        case ORR: // ORR
        {
            rd = reg[0];
            rn = reg[1];
            rm = reg[2];
            machineState.registers[rd] = machineState.registers[rn] | machineState.registers[rm];
            returnVal = 1;
            break;
        }
        case ORRI: // ORRI
        {
            rd = reg[0];
            rn = reg[1];
            im = reg[2];
            machineState.registers[rd] = machineState.registers[rn] | im;
            returnVal = 1;
            break;
        }
        case PRNL: // Prints a blank line
        {
            puts("");
            returnVal = 1;
            break;
        }
        case PRNT: // Prints out a register
        {
            rd = reg[0];
            printf("X%d\t%x\t%d", rd, machineState.registers[rd], machineState.registers[rd]);
            returnVal = 1;
            break;
        }
        case SDIV:
        {
            rd = reg[0];
            rn = reg[1];
            rm = reg[2];
            machineState.registers[rd] = machineState.registers[rn] / machineState.registers[rm];
            returnVal = 1;
            break;
        }
        case SMULH: // NOT REQUIRED; emulator will skip this instruction
        {
            returnVal = 1;
            break;
        }
        case STUR: // STUR
        {
            rd = reg[0];
            rn = reg[1];
            adr = reg[2];
            memcpy(
                machineState.memory + machineState.registers[rn] + adr, 
                &machineState.registers[rd], 
                sizeof(machineState.registers[rd])
            );
            returnVal = 1;
            break;
        }
        case STURB: // NOT REQUIRED; emulator will skip this instruction
        {
            returnVal = 1;
            break;
        }
        case STURH: // NOT REQUIRED; emulator will skip this instruction
        {
            returnVal = 1;
            break;
        }
        case STURW: // NOT REQUIRED; emulator will skip this instruction
        {
            returnVal = 1;
            break;
        }
        case SUB: // SUB
        {
            rd = reg[0];
            rn = reg[1];
            rm = reg[2];
            int32_t result = machineState.registers[rn] - machineState.registers[rm];
            machineState.registers[rd] = result;
            returnVal = 1;
            break;
        }
        case SUBI: // SUBI
        {
            rd = reg[0];
            rn = reg[1];
            im = reg[2];
            machineState.registers[rd] = machineState.registers[rn] - im;
            returnVal = 1;
            break;
        }
        case SUBIS: // SUBIS
        {
            rd = reg[0];
            rn = reg[1];
            im = reg[2];
            int32_t result = machineState.registers[rn] - im;
            machineState.registers[rd] = result;
            setConditionRegisters(result);
            returnVal = 1;
            break;
        }
        case SUBS: // SUBS
        {
            rd = reg[0];
            rn = reg[1];
            rm = reg[2];
            int32_t result = machineState.registers[rn] - machineState.registers[rm];
            machineState.registers[rd] = result;
            setConditionRegisters(result);
            returnVal = 1;
            break;
        }
        case UDIV: // UDIV
        {
            rd = reg[0];
            rn = reg[1];
            rm = reg[2];
            machineState.registers[rd] = machineState.registers[rn] / machineState.registers[rm];
            returnVal = 1;
            break;
        }
        case UMULH: // NOT REQUIRED; emulator will skip this instruction
        {
            returnVal = 1;
            break;
        }
        default: // Terminate program
        {
            machineState.pc = -1;
            returnVal = 0;
            break;
        }
    }

    // Check for data hazard
    if (lastLoadedRegister >= 0 && 
        (lastLoadedRegister == rn || lastLoadedRegister == rm || lastLoadedRegister == rd))
    {
        dataHazards++;
    }

    lastLoadedRegister = rd;
    return returnVal;
}

void printValue(int64_t v)
{
    //int64_t reg = machineState.registers[r];
    //printf("X%02d\t", v);
    printf("%08x\t", v);
    printf("%32s\t", int32tobin(v, false));
    printf("(%d)\t", v);
}

void printDump()
{
    printf("----------------------------------------------------\n");
    printf("Registers\n");
    printf("----------------------------------------------------\n");
    printf("Reg#\tHex\t\tBinary\t\t\t\t\tBase 10\n");
    for (int i = 0; i < 32; i++)
    {
        printf("X%d\t", i);
        printValue(machineState.registers[i]);
        puts("");
    }

    printf("----------------------------------------------------\n");
    printf("Stack\n");
    printf("----------------------------------------------------\n");
    printf("Location\tHex\t\tBinary\t\t\t\t\tBase 10\n");
    for (int i = 0; i < 512; i += 8)
    {
        int64_t s = *(int64_t*)(machineState.stack + i);
        printf("%08x\t", i);
        printValue(s);
        puts("");
    }

    printf("----------------------------------------------------\n");
    printf("Main Memory\n");
    printf("----------------------------------------------------\n");
    printf("Location\tHex\t\tBinary\t\t\t\t\tBase 10\n");
    for (int i = 0; i < 4096; i += 8)
    {
        int64_t s = *(int64_t*)(machineState.memory + i);
        printf("%08x\t", i);
        printValue(s);
        puts("");
    }


}

/** Sets the condition registers based on given subtraction result */
void setConditionRegisters(int32_t result)
{
    // Reset all condition registers
    for (int i = cEQ; i <= cVS; i++)
    {
        machineState.conditionRegisters[i] = 0;
    }

    if (result == 0)
    {
        machineState.conditionRegisters[cEQ] = 1;
    }
    if (result >= 0)
    {
        machineState.conditionRegisters[cGE] = 1;
    }
    if (result > 0)
    {
        machineState.conditionRegisters[cGT] = 1;
    }
    if (result > 0)
    {
        machineState.conditionRegisters[cHI] = 1;
    }
    if (result >= 0)
    {
        machineState.conditionRegisters[cHS] = 1;
    }
    if (result <= 0)
    {
        machineState.conditionRegisters[cLE] = 1;
    }
    if (result < 0)
    {
        machineState.conditionRegisters[cLO] = 1;
    }
    if (result <= 0)
    {
        machineState.conditionRegisters[cLS] = 1;
    }
    if (result < 0)
    {
        machineState.conditionRegisters[cLT] = 1;
    }
    if (result < 0)
    {
        machineState.conditionRegisters[cMI] = 1;
    }
    if (result != 0)
    {
        machineState.conditionRegisters[cNE] = 1;
    }
    if (result >= 0)
    {
        machineState.conditionRegisters[cPL] = 1;
    }
}

char* int32tobin(int32_t n, bool useSpace)
{
    char* binary = malloc(sizeof(char) * (32 + 4));
    for (int i = 31; i >= 0; i--)
    {
        int k = n >> i;
        if (k & 1)
        {
            strcat(binary, "1");
        }
        else
        {
            strcat(binary, "0");
        }

        if (useSpace && i % 8 == 0)
        {
            strcat(binary, " ");
        }
    }
    
    return binary;
}

/** Finds matching instruction in list and returns its index */
int findInstruction(uint32_t instruction)
{
    for (int i = 0; i < 37; i++)
    {
        // B instructions
        if (machineState.instructionOpcodes[i] == 
            ((instruction >> (32 - 6)) & 0b111111))
        {
            return i;
        }
        // CB instructions
        else if (machineState.instructionOpcodes[i] == 
            ((instruction >> (32 - 8)) & 0b11111111))
        {
            return i;
        }
        // I instructions
        else if (machineState.instructionOpcodes[i] == 
            ((instruction >> (32 - 10)) & 0b1111111111))
        {
            return i;
        }
        // R instructions
        else if (machineState.instructionOpcodes[i] == 
            ((instruction >> (32 - 11)) & 0b11111111111))
        {
            return i;
        }
        // D instructions
        else if (machineState.instructionOpcodes[i] == 
            ((instruction >> (32 - 11)) & 0b11111111111))
        {
            return i;
        }
    }

    return -1;
}

/** 
 * Parse register values from instruction binary
 * Returns an integer array with [0]:Rd, [1]:Rn, [2]:Rm/im
 */
int32_t* parseRegisters(uint32_t instruction)
{
    // Get format of given instruction
    int instructionIndex = findInstruction(instruction);
    int32_t opcode = machineState.instructionOpcodes[instructionIndex];
    int format = machineState.instructionFormats[instructionIndex];
    int32_t* reg = malloc(4 * sizeof(int64_t)); // reg[0] = Rd, reg[1] = Rn, reg[2] = Rm/im
    reg[0] = -1;
    reg[1] = -1;
    reg[2] = -1;

    int32_t rd, rn, rm, im, adr, shamt;

    // TODO change binary to hex
    switch (format)
    {
        case fR: // 11 opcode, 5 Rm, 6 shamt, 5 Rn, 5 Rd
        {
            rd = instruction & 0b11111;
            rn = (instruction >> 5) & 0b11111;
            shamt = (instruction >> (5 + 5)) & 0b111111;
            rm = (instruction >> (5 + 5 + 6)) & 0b11111;
            reg[0] = rd;
            reg[1] = rn;
            reg[2] = rm;
            reg[3] = shamt;
            break;
        }
        case fI: // 10 opcode, 12 immediate, 5 Rn, 5 Rd
        {
            rd = instruction & 0b11111;
            rn = (instruction >> 5) & 0b11111;
            im = (instruction >> (5 + 5)) & 0b111111111111;
            reg[0] = rd;
            reg[1] = rn;
            reg[2] = im;
            break;
        }
        case fD: // 11 opcode, 9 address (offset), 2 op, 5 Rn, 5 Rt
        {
            rd = instruction & 0b11111;
            rn = (instruction >> 5) & 0b11111;
            adr = (instruction >> (5 + 5 + 2)) & 0b111111111;
            reg[0] = rd;
            reg[1] = rn;
            reg[2] = adr;
            break;
        }
        case fB: // 6 opcode, 26 address
        {
            adr = instruction & 0b11111111111111111111111111;
            // Check for negative
            if ((instruction >> 5) & 0b10000000000000000000000000)
            {
                adr = -((~instruction >> 5) & 0b01111111111111111111111111) - 1;
            }
            reg[0] = adr;
            break;
        }
        case fCB: // 8 opcode, 19 address, 5 Rt
        {
            rd = instruction & 0b11111;
            adr = (instruction >> 5) & 0b1111111111111111111;
            // Check for negative
            if ((instruction >> 5) & 0b1000000000000000000)
            {
                adr = -((~instruction >> 5) & 0b0111111111111111111) - 1;
            }
            reg[0] = rd;
            reg[1] = adr;
            break;
        }
        case fIW: // 11 opcode, 16 immediate, 5 Rd
        {
            rd = instruction & 0b11111;
            im = (instruction >> 5) & 0b1111111111111111;
            break;
        }
        default:
        {
            break;
        }
    }

    return reg;
}