#ifndef INSTRUCTIONUTIL_H
# define INSTRUCTIONUTIL_H

#include "machine.h"
#include "main.h"

#include <stdbool.h>
#include <stdint.h>

struct MachineState machineState;

int32_t runInstruction(uint32_t instruction);
char* int32tobin(int32_t n, bool useSpace);
int findInstruction(uint32_t instruction);
int32_t* parseRegisters(uint32_t instruction);
void printDump();
void setConditionRegisters(int32_t result);

uint32_t lastLoadedRegister;

#endif