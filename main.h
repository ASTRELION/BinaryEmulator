#ifndef MAIN_H
# define MAIN_H

#include "machine.h"

struct MachineState machineState;

int instructionsExecuted;
int dataHazards;
int controlHazards;

#endif